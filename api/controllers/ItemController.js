/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	create: function(req, res){
		Item.createEach(req.body).meta({fetch: true})
				.then(function(createdItem){
					return ResponseService.json(200, res, 'Item created successfully', createdItem);
				})
				.catch(function(err){
					return ValidationService.jsonResolveError(err, Item, res);
			});
	},

	view: function(req, res){
		Item.findOne({ _id: req.params.id, isDeleted: false })
				.then(function(item){
		            if (_.isEmpty(item)) {
                		return ResponseService.json(404, res, "Item not found");
            		}
					return ResponseService.json(200, res, 'Item retrieved successfully', item);
				})
				.catch(function(err){
					return ValidationService.jsonResolveError(err, Item, res);
				});

	},

	list: function(req, res){
		Item.find({ isDeleted: false })
				.then(function(items){
					return ResponseService.json(200, res, 'Items retrieved successfully', items);
				})
				.catch(function(err){
					return ValidationService.jsonResolveError(err, Item, res);
				});

	},

	update: function(req, res){
		Item.update({ _id: req.params.id, isDeleted: false }, req.body).meta({fetch: true})
				.then(function(updatedItem){
				    if (!updatedItem.length) {
                		return ResponseService.json(404, res, "Item not found");
           		 	}
					return ResponseService.json(200, res, 'Item updated successfully', updatedItem[0]);
				})
				.catch(function(err){
					return ValidationService.jsonResolveError(err, Item, res);
				})
	},

	delete: function(req, res){
		Item.softDelete({ _id: req.params.id, isDeleted: false }).meta({fetch: true})
				.then(function(deletedItem){
					if (!deletedItem.length) {
                		return ResponseService.json(404, res, "Item not found");
            		}
					return ResponseService.json(200, res, 'Item deleted successfully', deletedItem[0]);
				})
				.catch(function(err){
					return ValidationService.jsonResolveError(err, Item, res);
				});
	}
};