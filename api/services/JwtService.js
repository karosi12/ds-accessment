/*
* Jwt Service
*/

var jwt = require("jsonwebtoken");
var jwtSecret = sails.config.settings.jwtSecret ? sails.config.settings.jwtSecret : sails.config.env.production.settings.jwtSecret;
var expiry = sails.config.settings.tokenExpiresIn ? sails.config.settings.tokenExpiresIn : sails.config.env.production.settings.tokenExpiresIn;

module.exports = {
	generateToken: function(payload) {
		var token = jwt.sign(payload, jwtSecret, { expiresIn: expiry });
		return token;
	},

	decodeToken: function(token, callback) {
		jwt.verify(token, jwtSecret, function(err, decoded){
			if (err) return callback(err);
			return callback(false, decoded);
		});
	}
};


