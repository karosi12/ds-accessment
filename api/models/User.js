/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var bcrypt = require('bcryptjs');

module.exports = {

  attributes: {
    userName: {
      type:'string',
      required: true,
      unique: true
    },
    firstName: {
      type:'string',
      required: true
    },
    lastName: {
      type:'string',
      required: true
    },
    gender: {
      type:'string',
      isIn: ['Male', 'Female']
    },
    role: {
      type: 'string',
      isIn: ['Admin', 'Regular'],
      defaultsTo: 'Regular'
    },
    password: {
      type:'string',
      required: true,
      minLength: 6
    },
    email: {
      type: 'string',
      unique: true,
      required: true,
      isEmail: true
    },
    phoneNumber: {
      type: 'string',
      required: true,
      unique: true
    },
    isDeleted: {
      type: 'boolean',
      defaultsTo: false
    },
  },
  methods: {
    validPassword(password, callback) {
      var obj = this.toObject();
      if (callback) {
        return bcrypt.compare(password, obj.password, callback);
      }
      return bcrypt.compareSync(password, obj.password);
    },
    toJSON: function(){
      var obj = _.clone(this);
      delete obj.password;
      return obj;
    },
  },
  softDelete: function(criteria){
    return this.update(criteria, { isDeleted : true });
  },
  hashPassword: function(password){
    if(!_.isNull(password)){
      password = bcrypt.hashSync(password, 8);
    }
    return password;
  },
  validationMessages: {
    userName: {
      required: 'Username is required',
      unique: 'Username is already registered'
    },
    firstName: {
      required: 'First Name is required'
    },
    lastName: {
      required: 'Last Name is required'
    },
    gender: {
      in: 'Gender should be either Male or Female'
    },
    password:{
      required: 'Password is required',
      minLength : 'Password needs at least 6 characters'
    },
    email: {
      required: 'Email is required',
      email: 'Email should be of the form yourname@example.com',
      unique: 'Email is already registered'
    },
    phoneNumber: {
      required: 'Phone number is required',
      unique: 'Phone number is already registered'
    }
  },
  beforeCreate: function(values, next){
    if(!_.isNull(values.password)){
      var hash = User.hashPassword(values.password);
      values.password = hash;
    }
    next();
  }
};