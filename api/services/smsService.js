var accountSid = "AC456828c9d34e7682361e649c2f66fc05"; // Your Account SID from www.twilio.com/console
var authToken = "b376c4ba82e5ebe9b8754293170bc12e"; // Your Auth Token from www.twilio.com/console

var twilio = require("twilio");
var client = new twilio(accountSid, authToken);

exports.sendSMS = (message, phoneNumber) => {
    console.log(phoneNumber);
  return client.messages.create({
    body: message,
    to: phoneNumber, // Text this number
    from: "+2347053310301" // From a valid Twilio number
  });
};
