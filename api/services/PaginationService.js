/**
 * PaginationService.js
 */
    
module.exports = {
    paginate: function (model, res, currentpage, perpage, criteria) {
        var pagination = {
            page: parseInt(currentpage) || 1,
            limit: parseInt(perpage) || 50
        };
        var params = criteria || {isDeleted: false};
        model.count(params).then(function (count){
            var findQuery = model.find(params).populateAll({isDeleted: false}).paginate(pagination);
            return [count, findQuery];
        })
        .spread(function (count, data){
            if (data.length) {
                var numberOfPages = Math.ceil(count / pagination.limit);
                var nextPage = parseInt(pagination.page) + 1;
                var meta = {
                    page: pagination.page,
                    perPage: pagination.limit,
                    previousPage: (pagination.page > 1) ? parseInt(pagination.page) - 1 : false,
                    nextPage: (numberOfPages >= nextPage) ? nextPage : false,
                    pageCount: numberOfPages,
                    total: count
                }
                return ResponseService.json(200, res, 'Records retrieved successfully', data, meta);
            } else {
                return ResponseService.json(200, res, 'No results found.', []);
            }
        })
        .catch(function (err){
          return ValidationService.jsonResolveError(err, model, res);
	   });
    }
    
};
