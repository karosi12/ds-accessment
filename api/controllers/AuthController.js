/**
 * AuthControllerController
 *
 * @description :: Server-side logic for managing Authcontrollers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const passport = require('passport'),
  jwtToken = require("../services/JwtService");


module.exports = {
  login: function (req, res) {
    passport.authenticate('local', function (err, user, info) {
      if ((err) || (!user)) {
        return res.send({
          message: info.message,
          user
        });
      }
      req.logIn(user, function (err) {
        const token = jwtToken.generateToken(user);
        user.token = token;
        if (err) res.send(err);
        jwtToken.decodeToken(token, function (err, decoded) {
          user.expiry = decoded.exp;
          return res.send({
            message: info.message,
            user
          });

          // return ResponseService.json(info.status, res, info.message, { user: user, token: token, expiry: decoded.exp });
        });
      });
    })(req, res);
  },
  logout: function (req, res) {
    req.logout();
    res.redirect('/');
  }
};
