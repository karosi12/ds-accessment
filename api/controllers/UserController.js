/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const mail = require("../services/mailService");
const sms = require("../services/smsService");

module.exports = {
  signup: function(req, res) {
    User.create(req.body)
      .meta({ fetch: true })
      .then(function(createdUser) {
        const mailOptions = {
          to: createdUser.email,
          subject: "Inventory Management",
          text: `Hello World`
        };

        return [mail.send(mailOptions), sms.sendSMS(mailOptions.text, createdUser.phoneNumber), createdUser];
      })
      .spread(function(mailResponse, smsResponse, createdUser) {
        return ResponseService.json(
          200,
          res,
          "User created successfully",
          createdUser
        );
      })
      .catch(function(err) {
        return ValidationService.jsonResolveError(err, User, res);
      });
  },

  view: function(req, res) {
    User.findOne({ _id: req.params.id, isDeleted: false })
      .then(function(user) {
        if (_.isEmpty(user)) {
          return ResponseService.json(404, res, "User not found");
        }
        return ResponseService.json(
          200,
          res,
          "User retrieved successfully",
          user
        );
      })
      .catch(function(err) {
        return ValidationService.jsonResolveError(err, User, res);
      });
  },

  list: function(req, res) {
    User.find({ isDeleted: false })
      .then(function(users) {
        return ResponseService.json(
          200,
          res,
          "Users retrieved successfully",
          users
        );
      })
      .catch(function(err) {
        return ValidationService.jsonResolveError(err, User, res);
      });
  },

  update: function(req, res) {
    User.update({ _id: req.params.id, isDeleted: false }, req.body)
      .meta({ fetch: true })
      .then(function(updatedUser) {
        if (!updatedUser.length) {
          return ResponseService.json(404, res, "User not found");
        }
        return ResponseService.json(
          200,
          res,
          "User updated successfully",
          updatedUser[0]
        );
      })
      .catch(function(err) {
        return ValidationService.jsonResolveError(err, User, res);
      });
  },

  delete: function(req, res) {
    User.softDelete({ _id: req.params.id, isDeleted: false })
      .meta({ fetch: true })
      .then(function(deletedUser) {
        if (!deletedUser.length) {
          return ResponseService.json(404, res, "User not found");
        }
        return ResponseService.json(
          200,
          res,
          "User deleted successfully",
          deletedUser[0]
        );
      })
      .catch(function(err) {
        return ValidationService.jsonResolveError(err, User, res);
      });
  }
};
