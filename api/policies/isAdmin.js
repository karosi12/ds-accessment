/**
 * isCEO
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */
module.exports = function(req, res, next) {
    console.log(req.user);
    if (_.includes(req.user.role, 'Admin')) {
        return next();
    }

    return ResponseService.json(403, res, 'You are not permitted to perform this action');
};
