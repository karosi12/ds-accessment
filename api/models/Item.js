/**
 * Item.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    creator: {
      type: "string",
      required: true
    },
    name: {
      type: "string",
      required: true
    },
    description: {
      type: "string"
    },
    price: {
      type: "string",
      required: true
    },
    isDeleted: {
      type: "boolean",
      defaultsTo: false
    }
  },
  softDelete: function(criteria) {
    return this.update(criteria, { isDeleted: true });
  },
  validationMessages: {
    creator: {
      required: "Creator is required"
    },
    name: {
      required: "Item Name is required"
    },
    price: {
      required: "Price is required"
    }
  }
};
