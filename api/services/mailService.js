const Promise = require('bluebird');
const nodemailer = require('nodemailer');
const aws = require('aws-sdk');
const ses = require('node-ses'); 


const senderName = 'Inventory Management';
const senderEmail = 'adekayor@gmail.com';
const config = {
    accessKeyId: sails.config.AWS_ACCESS_KEY_ID,
    secretAccessKey: sails.config.AWS_SECRET_ACCESS_KEY,
    region: sails.config.AWS_REGION
};


const transporter = nodemailer.createTransport({
  SES: new aws.SES(config)
});

const formatEmailOptions = (options) => {
    options.from = options.from || `${senderName} <${senderEmail}>`;
    if (options.text) {
        options.altText = options.text;
    }

    if (options.html) {
        options.message = options.html;
    }

    return options;
};

exports.send = (options) => {
    return new Promise((resolve, reject) => {
        options = formatEmailOptions(options);

        return transporter.sendMail(options, (err, data, res) => {
            if (err) {
                return reject(err);
            }
            return resolve(data);
        });
    });
};
