/**
 * HealthCheck controller
 *
 * @description :: Check the status of containers
 */

module.exports = {

    healthCheck: function (req, res) {
        return ResponseService.json(200, res, "API's alive n kickin' :)");
    }

}