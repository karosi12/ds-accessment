const passport = require('passport'),
  LocalStrategy = require('passport-local').Strategy,
  bcrypt = require('bcryptjs');

passport.serializeUser(function (user, cb) {
  cb(null, user.id);
});

passport.deserializeUser(function (id, cb) {
  User.findOne({
    id
  }, function (err, user) {
    cb(err, users);
  });
});

passport.use(new LocalStrategy({
  usernameField: 'userName',
  passportField: 'password'
}, function (username, password, cb) {
  User.findOne({
    userName: username
  }, function (err, user) {
    if (err) return cb(err);
    if (!user) return cb(null, false, {
      message: 'Username not found'
    });

    bcrypt.compare(password, user.password, function (err, res) {
      if (!res) return cb(null, false, {
        message: 'Invalid Password'
      });
      let userDetails = {
        email: user.email,
        userName: user.userName,
        id: user.id
      };
      return cb(null, userDetails, {
        message: 'Login Succesful'
      });
    });
  });
}));
