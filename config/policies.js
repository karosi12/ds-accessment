/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {

  /***************************************************************************
   *                                                                          *
   * Default policy for all controllers and actions, unless overridden.       *
   * (`true` allows public access)                                            *
   *                                                                          *
   ***************************************************************************/

  // '*': true,

  UserController: {
    // By default, require requests to come from a logged-in user
    // (runs the policy in api/policies/authenticated.js)
    '*': 'authenticated',

    // Only allow admin users to delete other users
    // (runs the policy in api/policies/isAdmin.js)

    'delete': ['authenticated', 'isAdmin'],

    // Allow anyone to access the signup action, even if they're not logged in.
    'signup': true
  },

  ItemController: {
    '*': 'authenticated',

    'delete': ['authenticated', 'isAdmin'],
  }

};
